class CreateThoughts < ActiveRecord::Migration
  def change
    create_table :thoughts do |t|
      t.string :thought
      t.string :owner

      t.timestamps
    end
  end
end
