class CreateAdvices < ActiveRecord::Migration
  def change
    create_table :advices do |t|
      t.string :advice
      t.string :taken_from
      t.string :tag

      t.timestamps
    end
  end
end
