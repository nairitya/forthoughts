class ServiceController < ApplicationController
	before_filter :set_session

	def add_thought
		thought = Thought.new(:thought => params[:thought], :owner => params[:from])
		if(thought.save)
			render text: "thought saved !!"
		else
			render text: thought.errors.full_messages
		end
	end

	def getaddpage
		render :action => "add"
	end

	def home_page
		render :action => "home"
	end
end
