class Thought < ActiveRecord::Base

	validates_presence_of :thought
	validates_presence_of :owner
end
